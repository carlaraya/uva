#include <iostream>

using namespace std;

int main()
{
    int x;
    int n;
    while (scanf("%d", &n) == 1)
    {
        if (n == 0)
            break;
        int coach;
        int arr[n + 1];
        while (scanf("%d", &coach) == 1)
        {
            if (coach == 0)
                break;

            for (int i = 1; i <= n; i++)
            {
                arr[i] = 1;
            }
            arr[coach] = 0;
            int ptr = coach - 1;

            int yesorno = 1;
            for (int i = 1; i < n; i++)
            {
                x = scanf("%d", &coach);
                if (ptr > coach)
                {
                    yesorno = 0;
                    i++;
                    while (i < n)
                    {
                        x = scanf("%d", &coach);
                        i++;
                    }
                    break;
                }
                else
                {
                    ptr = coach;
                    arr[coach] = 0;
                    for (ptr--; arr[ptr] == 0; ptr--);
                }
            }
            if (yesorno)
                cout << "Yes\n";
            else
                cout << "No\n";
        }
        cout << "\n";
    }
    return 0;
}

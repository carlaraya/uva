#include <iostream>

using namespace std;

int main()
{
    int n;
    while (scanf("%d", &n) == 1)
    {
        if (n == 0) break;
        int t[n][3];
        for (int i = 0; i < n; i++)
        {
            int x = scanf("%d%d", &t[i][0], &t[i][1]);
            t[i][2] = 0;
        }

        for (int i = 0; i < n; i++)
        {
            int largest = 0;
            int largestPos;
            for (int j = 0; j < n; j++)
            {
                if (!t[j][2] && t[j][0] > largest)
                {
                    largest = t[j][0];
                    largestPos = j;
                }
            }
            int ismoredarkmatter = 0;
            for (int j = largestPos - 1; j >= 0; j--)
            {
                if (t[j][1] > 0)
                {
                    ismoredarkmatter = 1;
                    t[largestPos][0] /= 2;
                    t[j][1]--;
                    break;
                }
            }
            if (!ismoredarkmatter)
            {
                for (int j = largestPos; j < n; j++)
                {
                    if (t[j][1] > 0)
                    {
                        ismoredarkmatter = 1;
                        break;
                    }
                }
                if (!ismoredarkmatter)
                {
                    break;
                }
            }
            t[largestPos][2] = 1;
        }

        int sum = 0;
        for (int i = 0; i < n; i++)
        {
            sum += t[i][0];
        }
        cout << sum << "\n";
    }
    return 0;
}

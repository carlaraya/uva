#include <iostream>

using namespace std;

int main()
{
    int T, N;
    cin >> T; 
    for (int i = 1; i <= T; i++)
    {
        cin >> N;
        int high = 0, low = 0, curr, last;
        cin >> last;
        for (int j = 1; j < N; j++)
        {
            cin >> curr;
            if (curr > last)
                high++;
            else if (curr < last)
                low++;
            last = curr;
        }
        cout << "Case " << i << ": " << high << " " << low << "\n";
    }
    return 0;
}

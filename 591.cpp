#include <iostream>

using namespace std;

int main()
{
    int x;
    int set = 1;
    int n;
    while (scanf("%d", &n) == 1)
    {
        if (n == 0) break;
        int arr[n];
        int aver = 0;
        for (int i = 0; i < n; i++)
        {
            x = scanf("%d", &arr[i]);
            aver += arr[i];
        }
        aver /= n;
        int moves = 0;
        for (int i = 0; i < n; i++)
        {
            if (arr[i] > aver)
            {
                moves += arr[i] - aver;
            }
        }
        cout << "Set #" << set << "\n";
        cout << "The minimum number of moves is " << moves << ".\n\n";
        set++;
    }
    return 0;
}

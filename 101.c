#include <stdio.h>
#include <stdlib.h>

int *find_n(int size, int **space, int n);
void put_back_orig_pos(int **space, int *aptr);
void print_space(int size, int **space);

int main(void)
{
    int size;
    if (scanf("%d", &size) != 1)
        return 1;
    int i, j;
    int **space = malloc(size * sizeof(int *));
    for (i = 0; i < size; i++)
    {
        space[i] = malloc((size + 1) * sizeof(int));
        space[i][0] = i;
        for (j = 1; j < size + 1; j++)
           space[i][j] = -1;
    }

    char s1[5], s2[5];
    int a, b;
    while (scanf("%s", s1) == 1)
    {
        if (strcmp(s1, "quit") == 0 || scanf("%d%s%d", &a, s2, &b) != 3)
            break;
        int *aptr = find_n(size, space, a);
        int *bptr = find_n(size, space, b);

        int issamestack = 0;
        int i;
        for (i = 0; aptr[i] != -1; i++)
        {
            if (aptr[i] == b)
            {
                issamestack = 1;
                break;
            }
        }
        for (i = 0; bptr[i] != -1; i++)
        {
            if (bptr[i] == a)
            {
                issamestack = 1;
                break;
            }
        }
        if (issamestack)
            continue;

        if (strcmp(s1, "move") == 0)
        {
            put_back_orig_pos(space, aptr);
            if (strcmp(s2, "onto") == 0)
            {
                put_back_orig_pos(space, bptr);
                bptr++;
            }
            else
            {
                while (*bptr != -1)
                    bptr++;
            }
            *aptr = -1;
            *bptr = a;
        }
        else
        {
            if (strcmp(s2, "onto") == 0)
            {
                put_back_orig_pos(space, bptr);
                bptr++;
            }
            else
            {
                while (*bptr != -1)
                    bptr++;
            }
            while (*aptr != -1)
            {
                *bptr = *aptr;
                *aptr = -1;
                aptr++;
                bptr++;
            }
        }
    }

    print_space(size, space);
    for (i = 0; i < size; i++)
    {
        free(space[i]);
    }
    free(space);

    return 0;
}

/* return address of n found in space */
int *find_n(int size, int **space, int n)
{
    int i, j;
    for (i = 0; i < size; i++)
    {
        for (j = 0; j < size; j++)
        {
            if (space[i][j] == n)
                return &space[i][j];
        }
    }
    return NULL;
}

/* put everything above a back to original position */
void put_back_orig_pos(int **space, int *aptr)
{
    int i;
    for (i = 1; aptr[i] != -1; i++)
    {
        space[aptr[i]][0] = aptr[i];
        aptr[i] = -1;
    }
}

/* print space */
void print_space(int size, int **space)
{
    int i, j;
    for (i = 0; i < size; i++)
    {
        printf("%d:", i);
        for (j = 0; j < size; j++)
        {
            if (space[i][j] == -1)
                break;
            printf(" %d", space[i][j]);
        }
        printf("\n");
    }
}

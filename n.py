def func(b, m):
    ct = 1
    x = 1
    cycle = []
    while x not in cycle:
        cycle.append(x)
        x *= b
        x %= m
        ct += 1
    cycle.append(x)
    return cycle

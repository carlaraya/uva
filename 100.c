#include <stdio.h>

#define N 1000000

int arr[N + 1];

int find_cycle(int n);

int main(void)
{
    int x;
    for (x = 0; x <= N; x++)
        arr[x] = 0;
    arr[1] = 1;

    int i, j;
    while (scanf("%d%d", &i, &j) == 2)
    {
        int max = 0;
        printf("%d %d ", i, j);
        if (i > j)
        {
            int swap = i;
            i = j;
            j = swap;
        }
        for (; i <= j; i++)
        {
            int cycle = find_cycle(i);
            if (cycle == 0)
            {
                max = 0;
                break;
            }
            if (cycle > max)
                max = cycle;
        }
        if (max == 0)
        {
            printf("Invalid input\n");
        }
        else
        {
            printf("%d\n", max);
        }
    }
    return 0;
}

int find_cycle(int n)
{
    if (n <= N && arr[n] != 0)
        return arr[n];
    if (n == 1)
        return 1;
    int orig_n = n;
    if (n % 2)
    {
        n = 3 * n + 1;
        if (n < 0)
        {
            return 0;
        }
    }
    else
        n = n / 2;
    int x = find_cycle(n);
    if (x == 0)
        return 0;
    if (orig_n <= N && arr[orig_n] == 0)
    {
        arr[orig_n] = 1 + x;
    }
    return 1 + x;

}

#include <stdio.h>

/*
 * Compare 2 boxes a and b. Return -1, 1, or 0 if a fits in b, b fits in a, or
 * neither. Assume a and b are sorted.
 */
int boxcmp(int a[], int b[], int n);

int main(void)
{
    int i, j;

    int k, n;
    while (scanf("%d%d", &k, &n) == 2)
    {
        int boxes[k][n];
        int order[k];
        for (i = 0; i < k; i++)
        {
            order[i] = i;
            for (j = 0; j < n; j++)
            {
                int tmp;
                if (scanf("%d", &tmp) != 1) return 1;
                int a;
                for (a = j; a > 0 && boxes[i][a - 1] > tmp; a--)
                    boxes[i][a] = boxes[i][a - 1];
                boxes[i][a] = tmp;
            }
        }
        /* <algorithm> */

        /* </algorithm> */
    }
    return 0;
}

int boxcmp(int a[], int b[], int n)
{
    int i;
    if (a[0] < b[0])
    {
        for (i = 1; i < n; i++)
            if (a[i] >= b[i])
                return 0;
        return -1
    }
    else if (a[0] > b[0])
    {
        for (i = 1; i < n; i++)
            if (a[i] <= b[i])
                return 0;
        return 1
    }
    else
        return 0;
}

/*
 * PRINT BOXES
for (i = 0; i < k; i++)
{
    for (j = 0; j < n; j++)
        printf("%d ", boxes[i][j]);
    printf("\n");
}
*/

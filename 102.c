#include <stdio.h>

typedef unsigned int uint;

int main(void)
{
    uint a0[3];
    uint a1[3];
    uint a2[3];
    char *letters = "BGC";
    int perms[][3] = {
        {0,2,1},
        {0,1,2},
        {2,0,1},
        {2,1,0},
        {1,0,2},
        {1,2,0}
    };
    while (scanf("%u%u%u%u%u%u%u%u%u", a0, a0+1, a0+2, a1, a1+1, a1+2, a2, a2+1, a2+2) == 9)
    {
        uint max = 0;
        int maxi;
        uint curr;
        uint sum = a0[0] + a0[1] + a0[2] + a1[0] + a1[1] + a1[2] + a2[0] + a2[1] + a2[2];
        int i;
        for (i = 0; i < 6; i++)
        {
            curr = a0[perms[i][0]] + a1[perms[i][1]] + a2[perms[i][2]];
            if (max < curr)
            {
                max = curr;
                maxi = i;
            }
        }
        printf("%c%c%c %u\n", letters[perms[maxi][0]], letters[perms[maxi][1]], letters[perms[maxi][2]], sum - max);
    }
    return 0;
}

#include <stdio.h>

int main(void)
{
    int n;
    while (scanf("%d", &n) == 1)
    {
        if (n == 0) break;
        int t[n][3];
        int i;
        for (i = 0; i < n; i++)
        {
            int x = scanf("%d%d", &t[i][0], &t[i][1]);
            t[i][2] = 0;
        }

        for (i = 0; i < n; i++)
        {
            int largest = 0;
            int largestPos;
            int j;
            for (j = 0; j < n; j++)
            {
                if (!t[j][2] && t[j][0] > largest)
                {
                    largest = t[j][0];
                    largestPos = j;
                }
            }
            for (j = largestPos - 1; j >= 0; j--)
            {
                if (t[j][1] > 0)
                {
                    t[largestPos][0] /= 2;
                    t[j][1]--;
                    break;
                }
            }
            t[largestPos][2] = 1;
        }

        int sum = 0;
        for (i = 0; i < n; i++)
        {
            sum += t[i][0];
        }
        printf("%d\n", sum);
    }
    return 0;
}
